(uiop/package:define-package stacks-openapi-client (:use)
                             (:export #:*bearer* #:*authorization* #:*headers*
                              #:*cookie* #:*parse* #:*server*
                              "GET-POOL-DELEGATIONS" "GET-FILTERED-EVENTS"
                              "POST-FEE-TRANSACTION" "FETCH-FEE-RATE"
                              "GET-NFT-MINTS" "GET-NFT-HISTORY"
                              "GET-NFT-HOLDINGS"
                              "GET-ADDRESS-MEMPOOL-TRANSACTIONS"
                              "GET-TRANSACTIONS-BY-BLOCK-HEIGHT"
                              "GET-TRANSACTIONS-BY-BLOCK-HASH"
                              "GET-NAMES-OWNED-BY-ADDRESS"
                              "GET-HISTORICAL-ZONE-FILE" "FETCH-ZONE-FILE"
                              "FETCH-SUBDOMAINS-LIST-FOR-NAME" "GET-NAME-INFO"
                              "GET-ALL-NAMES" "GET-NAMESPACE-NAMES"
                              "GET-ALL-NAMESPACES" "GET-NAME-PRICE"
                              "GET-NAMESPACE-PRICE"
                              "ROSETTA-CONSTRUCTION-COMBINE"
                              "ROSETTA-CONSTRUCTION-PAYLOADS"
                              "ROSETTA-CONSTRUCTION-SUBMIT"
                              "ROSETTA-CONSTRUCTION-PREPROCESS"
                              "ROSETTA-CONSTRUCTION-PARSE"
                              "ROSETTA-CONSTRUCTION-METADATA"
                              "ROSETTA-CONSTRUCTION-HASH"
                              "ROSETTA-CONSTRUCTION-DERIVE"
                              "ROSETTA-MEMPOOL-TRANSACTION" "ROSETTA-MEMPOOL"
                              "ROSETTA-BLOCK-TRANSACTION" "ROSETTA-BLOCK"
                              "ROSETTA-ACCOUNT-BALANCE"
                              "ROSETTA-NETWORK-STATUS"
                              "ROSETTA-NETWORK-OPTIONS" "ROSETTA-NETWORK-LIST"
                              "SEARCH-BY-ID" "GET-POX-INFO"
                              "GET-TOTAL-STX-SUPPLY-LEGACY-FORMAT"
                              "GET-STX-SUPPLY-CIRCULATING-PLAIN"
                              "GET-STX-SUPPLY-TOTAL-SUPPLY-PLAIN"
                              "GET-STX-SUPPLY"
                              "GET-NETWORK-BLOCK-TIME-BY-NETWORK"
                              "GET-NETWORK-BLOCK-TIMES" "GET-STATUS"
                              "GET-CORE-API-INFO" "GET-FEE-TRANSFER"
                              "GET-ACCOUNT-INFO" "GET-ACCOUNT-INBOUND"
                              "GET-ACCOUNT-ASSETS" "GET-ACCOUNT-NONCES"
                              "GET-ACCOUNT-TRANSACTIONS-WITH-TRANSFERS"
                              "GET-SINGLE-TRANSACTION-WITH-TRANSFERS"
                              "GET-ACCOUNT-TRANSACTIONS"
                              "GET-ACCOUNT-STX-BALANCE" "GET-ACCOUNT-BALANCE"
                              "CALL-READ-ONLY-FUNCTION" "GET-CONTRACT-SOURCE"
                              "GET-CONTRACT-DATA-MAP-ENTRY"
                              "GET-CONTRACT-INTERFACE"
                              "GET-CONTRACT-EVENTS-BY-ID"
                              "GET-CONTRACTS-BY-TRAIT" "GET-CONTRACT-BY-ID"
                              "GET-BURNCHAIN-REWARDS-TOTAL-BY-ADDRESS"
                              "GET-BURNCHAIN-REWARD-LIST-BY-ADDRESS"
                              "GET-BURNCHAIN-REWARD-LIST"
                              "GET-BURNCHAIN-REWARD-SLOT-HOLDERS-BY-ADDRESS"
                              "GET-BURNCHAIN-REWARD-SLOT-HOLDERS"
                              "GET-BLOCK-BY-BURN-BLOCK-HEIGHT"
                              "GET-BLOCK-BY-BURN-BLOCK-HASH"
                              "GET-BLOCK-BY-HEIGHT" "GET-BLOCK-BY-HASH"
                              "GET-BLOCK-LIST" "GET-POX-CYCLE-SIGNER-STACKERS"
                              "GET-POX-CYCLE-SIGNER" "GET-POX-CYCLE-SIGNERS"
                              "GET-POX-CYCLE" "GET-POX-CYCLES"
                              "GET-SMART-CONTRACTS-STATUS"
                              "GET-ADDRESS-TRANSACTION-EVENTS"
                              "GET-ADDRESS-TRANSACTIONS"
                              "GET-TRANSACTIONS-BY-BLOCK" "GET-BLOCK"
                              "GET-AVERAGE-BLOCK-TIMES" "GET-BLOCKS"
                              "GET-BLOCKS-BY-BURN-BLOCK" "GET-BURN-BLOCK"
                              "GET-BURN-BLOCKS" "GET-UNANCHORED-TXS"
                              "GET-MICROBLOCK-BY-HASH" "GET-MICROBLOCK-LIST"
                              "POST-CORE-NODE-TRANSACTIONS"
                              "GET-RAW-TRANSACTION-BY-ID"
                              "GET-TRANSACTION-BY-ID" "GET-TX-LIST-DETAILS"
                              "GET-MEMPOOL-TRANSACTION-STATS"
                              "GET-DROPPED-MEMPOOL-TRANSACTION-LIST"
                              "GET-MEMPOOL-FEE-PRIORITIES"
                              "GET-MEMPOOL-TRANSACTION-LIST"
                              "GET-TRANSACTION-LIST" "RUN-FAUCET-BTC"
                              "RUN-FAUCET-STX"))

(cl:in-package :stacks-openapi-client)



(common-lisp:defparameter *parse* common-lisp:nil)
(common-lisp:defparameter *authorization* common-lisp:nil)
(common-lisp:defparameter *bearer* common-lisp:nil)
(common-lisp:defparameter *server* common-lisp:nil)
(common-lisp:defparameter *cookie* common-lisp:nil)
(common-lisp:defparameter *headers* 'common-lisp:nil)
(common-lisp:defparameter *query* 'common-lisp:nil)


(common-lisp:defun get/extended/beta/stacking/{pool_principal}/delegations
                   (pool-principal
                    common-lisp:&key (query *query*) (headers *headers*)
                    (cookie *cookie*) (authorization *authorization*)
                    (bearer *bearer*) (server *server*) (parse *parse*)
                    after-block unanchored limit offset)
  "Operation-id: get-pool-delegations
Summary: Stacking pool members
Description: Retrieves the list of stacking pool members for a given delegator principal."
  (serapeum:assuref pool-principal common-lisp:string)
  (common-lisp:when after-block
    (serapeum:assuref after-block common-lisp:integer))
  (common-lisp:when unanchored
    (serapeum:assuref unanchored common-lisp:boolean))
  (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
  (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
  (common-lisp:let* ((server-uri
                      (quri.uri:uri
                       (common-lisp:or server "https://api.mainnet.hiro.so")))
                     (response
                      (dexador.backend.usocket:request
                       (quri:render-uri
                        (quri:make-uri :scheme (quri.uri:uri-scheme server-uri)
                                       :host (quri.uri:uri-host server-uri)
                                       :port (quri.uri:uri-port server-uri)
                                       :path
                                       (str:concat
                                        (quri.uri:uri-path server-uri)
                                        (str:concat "/extended/beta/stacking/"
                                                    pool-principal
                                                    "/delegations"))
                                       :query
                                       (openapi-generator::remove-empty-values
                                        (common-lisp:append
                                         (common-lisp:list
                                          (common-lisp:cons "after_block"
                                                            after-block)
                                          (common-lisp:cons "unanchored"
                                                            unanchored)
                                          (common-lisp:cons "limit" limit)
                                          (common-lisp:cons "offset" offset))
                                         (common-lisp:when query query)))))
                       :method 'get :bearer-auth bearer :headers
                       (openapi-generator::remove-empty-values
                        (common-lisp:append
                         (common-lisp:list
                          (common-lisp:when authorization
                            (common-lisp:cons "Authorization" authorization))
                          (common-lisp:cons "cookie" cookie))
                         (common-lisp:when headers headers))))))
    (common-lisp:if parse
                    (com.inuoe.jzon:parse response)
                    response)))
 (common-lisp:defun get/extended/v1/tx/events
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) tx-id
                     address limit offset type)
   "Operation-id: get-filtered-events
Summary: Transaction Events
Description: Retrieves the list of events filtered by principal (STX address or Smart Contract ID), transaction id or event types. The list of event types is ('smart_contract_log', 'stx_lock', 'stx_asset', 'fungible_token_asset', 'non_fungible_token_asset')."
   (common-lisp:when tx-id (serapeum:assuref tx-id common-lisp:string))
   (common-lisp:when address (serapeum:assuref address common-lisp:string))
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when type (serapeum:assuref type common-lisp:array))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/tx/events")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "tx_id" tx-id)
                                           (common-lisp:cons "address" address)
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "type" type))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/v2/fees/transaction
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     transaction-fee-estimate-request transaction-payload
                     estimated-len content)
   "Operation-id: post-fee-transaction
Summary: Get approximate fees for a given transaction
Description: Get an estimated fee for the supplied transaction.  This
estimates the execution cost of the transaction, the current
fee rate of the network, and returns estimates for fee
amounts.
* `transaction_payload` is a hex-encoded serialization of
  the TransactionPayload for the transaction.
* `estimated_len` is an optional argument that provides the
  endpoint with an estimation of the final length (in bytes)
  of the transaction, including any post-conditions and
  signatures

If the node cannot provide an estimate for the transaction
(e.g., if the node has never seen a contract-call for the
given contract and function) or if estimation is not
configured on this node, a 400 response is returned.

The 400 response will be a JSON error containing a `reason`
field which can be one of the following:
* `DatabaseError` - this Stacks node has had an internal
  database error while trying to estimate the costs of the
  supplied transaction.
* `NoEstimateAvailable` - this Stacks node has not seen this
  kind of contract-call before, and it cannot provide an
  estimate yet.
* `CostEstimationDisabled` - this Stacks node does not perform
  fee or cost estimation, and it cannot respond on this
  endpoint.

The 200 response contains the following data:
* `estimated_cost` - the estimated multi-dimensional cost of
  executing the Clarity VM on the provided transaction.
* `estimated_cost_scalar` - a unitless integer that the Stacks
  node uses to compare how much of the block limit is consumed
  by different transactions. This value incorporates the
  estimated length of the transaction and the estimated
  execution cost of the transaction. The range of this integer
  may vary between different Stacks nodes. In order to compute
  an estimate of total fee amount for the transaction, this
  value is multiplied by the same Stacks node's estimated fee
  rate.
* `cost_scalar_change_by_byte` - a float value that indicates how
  much the `estimated_cost_scalar` value would increase for every
  additional byte in the final transaction.
* `estimations` - an array of estimated fee rates and total fees to
  pay in microSTX for the transaction. This array provides a range of
  estimates (default: 3) that may be used. Each element of the array
  contains the following fields:
    * `fee_rate` - the estimated value for the current fee
      rates in the network
    * `fee` - the estimated value for the total fee in
      microSTX that the given transaction should pay. These
      values are the result of computing:
      `fee_rate` x `estimated_cost_scalar`.
      If the estimated fees are less than the minimum relay
      fee `(1 ustx x estimated_len)`, then that minimum relay
      fee will be returned here instead.

Note: If the final transaction's byte size is larger than
supplied to `estimated_len`, then applications should increase
this fee amount by:
  `fee_rate` x `cost_scalar_change_by_byte` x (`final_size` - `estimated_size`)"
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/v2/fees/transaction")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (transaction-fee-estimate-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             transaction-fee-estimate-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           transaction-fee-estimate-request)
                                          transaction-fee-estimate-request))
                         ((common-lisp:or transaction-payload estimated-len)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key*
                                 "transaction_payload")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref transaction-payload
                                                    common-lisp:string)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse transaction-payload))
                                   transaction-payload)))
                                (common-lisp:when estimated-len
                                  (com.inuoe.jzon:write-key* "estimated_len")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref estimated-len
                                                     common-lisp:t)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/extended/v1/fee_rate
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     fee-rate-request transaction content)
   "Operation-id: fetch-fee-rate
Summary: Fetch fee rate
Description: **NOTE:** This endpoint is deprecated in favor of [Get approximate fees for a given transaction](/api/get-approximate-fees-for-a-given-transaction).

Retrieves estimated fee rate."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/fee_rate")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (fee-rate-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref fee-rate-request
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           fee-rate-request)
                                          fee-rate-request))
                         ((common-lisp:or transaction)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "transaction")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref transaction
                                                    common-lisp:string)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse transaction))
                                   transaction)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tokens/nft/mints
                    (asset-identifier
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset unanchored tx-metadata)
   "Operation-id: get-nft-mints
Summary: Non-Fungible Token mints
Description: Retrieves all mint events for a Non-Fungible Token asset class. Useful to determine which NFTs of a particular collection have been claimed.

More information on Non-Fungible Tokens on the Stacks blockchain can be found [here](https://docs.stacks.co/write-smart-contracts/tokens#non-fungible-tokens-nfts)."
   (serapeum:assuref asset-identifier common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:when tx-metadata
     (serapeum:assuref tx-metadata common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/tokens/nft/mints")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "asset_identifier"
                                                             asset-identifier)
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "unanchored"
                                                             unanchored)
                                           (common-lisp:cons "tx_metadata"
                                                             tx-metadata))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tokens/nft/history
                    (asset-identifier value
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset unanchored tx-metadata)
   "Operation-id: get-nft-history
Summary: Non-Fungible Token history
Description: Retrieves all events relevant to a Non-Fungible Token. Useful to determine the ownership history of a particular asset.

More information on Non-Fungible Tokens on the Stacks blockchain can be found [here](https://docs.stacks.co/write-smart-contracts/tokens#non-fungible-tokens-nfts)."
   (serapeum:assuref asset-identifier common-lisp:string)
   (serapeum:assuref value common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:when tx-metadata
     (serapeum:assuref tx-metadata common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/tokens/nft/history")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "asset_identifier"
                                                             asset-identifier)
                                           (common-lisp:cons "value" value)
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "unanchored"
                                                             unanchored)
                                           (common-lisp:cons "tx_metadata"
                                                             tx-metadata))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tokens/nft/holdings
                    (principal
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     asset-identifiers limit offset unanchored tx-metadata)
   "Operation-id: get-nft-holdings
Summary: Non-Fungible Token holdings
Description: Retrieves the list of Non-Fungible Tokens owned by the given principal (STX address or Smart Contract ID).
Results can be filtered by one or more asset identifiers and can include metadata about the transaction that made the principal own each token.

More information on Non-Fungible Tokens on the Stacks blockchain can be found [here](https://docs.stacks.co/write-smart-contracts/tokens#non-fungible-tokens-nfts)."
   (serapeum:assuref principal common-lisp:string)
   (common-lisp:when asset-identifiers
     (serapeum:assuref asset-identifiers common-lisp:array))
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:when tx-metadata
     (serapeum:assuref tx-metadata common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/tokens/nft/holdings")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "principal"
                                                             principal)
                                           (common-lisp:cons
                                            "asset_identifiers"
                                            asset-identifiers)
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "unanchored"
                                                             unanchored)
                                           (common-lisp:cons "tx_metadata"
                                                             tx-metadata))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/address/{address}/mempool
                    (address
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset unanchored)
   "Operation-id: get-address-mempool-transactions
Summary: Transactions for address
Description: Retrieves all transactions for a given address that are currently in mempool"
   (serapeum:assuref address common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/address/"
                                                     address "/mempool"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "unanchored"
                                                             unanchored))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tx/block_height/{height}
                    (height
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset unanchored)
   "Operation-id: get-transactions-by-block-height
Summary: Transactions by block height
Description: **NOTE:** This endpoint is deprecated in favor of [Get transactions by block](/api/get-transactions-by-block).

Retrieves all transactions within a block at a given height"
   (serapeum:assuref height common-lisp:integer)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat
                                          "/extended/v1/tx/block_height/"
                                          (common-lisp:write-to-string
                                           height)))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "unanchored"
                                                             unanchored))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tx/block/{block_hash}
                    (block-hash
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-transactions-by-block-hash
Summary: Transactions by block hash
Description: **NOTE:** This endpoint is deprecated in favor of [Get transactions by block](/api/get-transactions-by-block).

Retrieves a list of all transactions within a block for a given block hash."
   (serapeum:assuref block-hash common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/tx/block/"
                                                     block-hash))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v1/addresses/{blockchain}/{address}
                    (blockchain address
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-names-owned-by-address
Summary: Get Names Owned by Address
Description: Retrieves a list of names owned by the address provided."
   (serapeum:assuref blockchain common-lisp:string)
   (serapeum:assuref address common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v1/addresses/"
                                                     blockchain "/" address))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v1/names/{name}/zonefile/{zonefilehash}
                    (name zone-file-hash
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-historical-zone-file
Summary: Get Historical Zone File
Description: Retrieves the historical zonefile specified by the username and zone hash."
   (serapeum:assuref name common-lisp:string)
   (serapeum:assuref zone-file-hash common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v1/names/" name
                                                     "/zonefile/"
                                                     zone-file-hash))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v1/names/{name}/zonefile
                    (name
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: fetch-zone-file
Summary: Get Zone File
Description: Retrieves a user’s raw zone file. This only works for RFC-compliant zone files. This method returns an error for names that have non-standard zone files."
   (serapeum:assuref name common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v1/names/" name
                                                     "/zonefile"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v1/names/{name}/subdomains
                    (name
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: fetch-subdomains-list-for-name
Summary: Get Name Subdomains
Description: Retrieves the list of subdomains for a specific name"
   (serapeum:assuref name common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v1/names/" name
                                                     "/subdomains"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v1/names/{name}
                    (name
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-name-info
Summary: Get Name Details
Description: Retrieves details of a given name including the `address`, `status` and last transaction id - `last_txid`."
   (serapeum:assuref name common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v1/names/" name))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v1/names
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) page)
   "Operation-id: get-all-names
Summary: Get All Names
Description: Retrieves a list of all names known to the node."
   (common-lisp:when page (serapeum:assuref page common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/v1/names")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "page" page))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v1/namespaces/{tld}/names
                    (tld
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) page)
   "Operation-id: get-namespace-names
Summary: Get Namespace Names
Description: Retrieves a list of names within a given namespace."
   (serapeum:assuref tld common-lisp:string)
   (common-lisp:when page (serapeum:assuref page common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v1/namespaces/" tld
                                                     "/names"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "page" page))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v1/namespaces
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-all-namespaces
Summary: Get All Namespaces
Description: Retrieves a list of all namespaces known to the node."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/v1/namespaces")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v2/prices/names/{name}
                    (name
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-name-price
Summary: Get Name Price
Description: Retrieves the price of a name. The `amount` given will be in the smallest possible units of the currency."
   (serapeum:assuref name common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v2/prices/names/" name))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v2/prices/namespaces/{tld}
                    (tld
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-namespace-price
Summary: Get Namespace Price
Description: Retrieves the price of a namespace. The `amount` given will be in the smallest possible units of the currency."
   (serapeum:assuref tld common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v2/prices/namespaces/"
                                                     tld))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/construction/combine
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-construction-combine-request network-identifier
                     unsigned-transaction signatures content)
   "Operation-id: rosetta-construction-combine
Summary: Create Network Transaction from Signatures
Description: Take unsigned transaction and signature, combine both and return signed transaction. The examples below are illustrative only. You'll need to use your wallet to generate actual values to use them in the request payload."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/construction/combine")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-construction-combine-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-construction-combine-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-construction-combine-request)
                                          rosetta-construction-combine-request))
                         ((common-lisp:or network-identifier
                                          unsigned-transaction signatures)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key*
                                 "unsigned_transaction")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref unsigned-transaction
                                                    common-lisp:string)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse
                                     unsigned-transaction))
                                   unsigned-transaction)))
                                (com.inuoe.jzon:write-key* "signatures")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref signatures
                                                    (common-lisp:or
                                                     openapi-generator::json-array
                                                     common-lisp:list))
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse signatures))
                                   signatures)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/construction/payloads
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-construction-payloads-request network-identifier
                     operations public-keys metadata content)
   "Operation-id: rosetta-construction-payloads
Summary: Generate an Unsigned Transaction and Signing Payloads
Description: Generate an unsigned transaction from operations and metadata. The examples below are illustrative only. You'll need to use your wallet to generate actual values to use them in the request payload."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/construction/payloads")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-construction-payloads-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-construction-payloads-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-construction-payloads-request)
                                          rosetta-construction-payloads-request))
                         ((common-lisp:or network-identifier operations
                                          public-keys metadata)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key* "operations")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref operations
                                                    (common-lisp:or
                                                     openapi-generator::json-array
                                                     common-lisp:list))
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse operations))
                                   operations)))
                                (common-lisp:when public-keys
                                  (com.inuoe.jzon:write-key* "public_keys")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref public-keys
                                                     (common-lisp:or
                                                      openapi-generator::json-array
                                                      common-lisp:list))))
                                (common-lisp:when metadata
                                  (com.inuoe.jzon:write-key* "metadata")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref metadata
                                                     (common-lisp:or
                                                      openapi-generator::json-object
                                                      common-lisp:hash-table))))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/construction/submit
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-construction-submit-request network-identifier
                     signed-transaction content)
   "Operation-id: rosetta-construction-submit
Summary: Submit a Signed Transaction
Description: Submit a pre-signed transaction to the node. The examples below are illustrative only. You'll need to use your wallet to generate actual values to use them in the request payload."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/construction/submit")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-construction-submit-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-construction-submit-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-construction-submit-request)
                                          rosetta-construction-submit-request))
                         ((common-lisp:or network-identifier
                                          signed-transaction)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key* "signed_transaction")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref signed-transaction
                                                    common-lisp:string)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse signed-transaction))
                                   signed-transaction)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/construction/preprocess
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-construction-preprocess-request network-identifier
                     operations metadata max-fee suggested-fee-multiplier
                     content)
   "Operation-id: rosetta-construction-preprocess
Summary: Create a Request to Fetch Metadata
Description: TODO"
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/construction/preprocess")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-construction-preprocess-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-construction-preprocess-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-construction-preprocess-request)
                                          rosetta-construction-preprocess-request))
                         ((common-lisp:or network-identifier operations
                                          metadata max-fee
                                          suggested-fee-multiplier)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key* "operations")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref operations
                                                    (common-lisp:or
                                                     openapi-generator::json-array
                                                     common-lisp:list))
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse operations))
                                   operations)))
                                (common-lisp:when metadata
                                  (com.inuoe.jzon:write-key* "metadata")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref metadata
                                                     (common-lisp:or
                                                      openapi-generator::json-object
                                                      common-lisp:hash-table))))
                                (common-lisp:when max-fee
                                  (com.inuoe.jzon:write-key* "max_fee")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref max-fee
                                                     (common-lisp:or
                                                      openapi-generator::json-array
                                                      common-lisp:list))))
                                (common-lisp:when suggested-fee-multiplier
                                  (com.inuoe.jzon:write-key*
                                   "suggested_fee_multiplier")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref suggested-fee-multiplier
                                                     common-lisp:t)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/construction/parse
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-construction-parse-request network-identifier
                     signed transaction content)
   "Operation-id: rosetta-construction-parse
Summary: Parse a Transaction
Description: TODO"
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/construction/parse")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-construction-parse-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-construction-parse-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-construction-parse-request)
                                          rosetta-construction-parse-request))
                         ((common-lisp:or network-identifier signed
                                          transaction)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key* "signed")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref signed
                                                    (common-lisp:or
                                                     openapi-generator::json-true
                                                     openapi-generator::json-false
                                                     common-lisp:null
                                                     common-lisp:t))
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse signed))
                                   signed)))
                                (com.inuoe.jzon:write-key* "transaction")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref transaction
                                                    common-lisp:string)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse transaction))
                                   transaction)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/construction/metadata
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-construction-metadata-request network-identifier
                     options public-keys content)
   "Operation-id: rosetta-construction-metadata
Summary: Get Metadata for Transaction Construction
Description: To Do"
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/construction/metadata")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-construction-metadata-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-construction-metadata-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-construction-metadata-request)
                                          rosetta-construction-metadata-request))
                         ((common-lisp:or network-identifier options
                                          public-keys)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key* "options")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref options common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse options))
                                   options)))
                                (common-lisp:when public-keys
                                  (com.inuoe.jzon:write-key* "public_keys")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref public-keys
                                                     (common-lisp:or
                                                      openapi-generator::json-array
                                                      common-lisp:list))))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/construction/hash
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-construction-hash-request network-identifier
                     signed-transaction content)
   "Operation-id: rosetta-construction-hash
Summary: Get the Hash of a Signed Transaction
Description: Retrieves the network-specific transaction hash for a signed transaction."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/construction/hash")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-construction-hash-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-construction-hash-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-construction-hash-request)
                                          rosetta-construction-hash-request))
                         ((common-lisp:or network-identifier
                                          signed-transaction)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key* "signed_transaction")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref signed-transaction
                                                    common-lisp:string)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse signed-transaction))
                                   signed-transaction)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/construction/derive
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-construction-derive-request network-identifier
                     public-key metadata content)
   "Operation-id: rosetta-construction-derive
Summary: Derive an AccountIdentifier from a PublicKey
Description: Retrieves the Account Identifier information based on a Public Key for a given network"
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/construction/derive")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-construction-derive-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-construction-derive-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-construction-derive-request)
                                          rosetta-construction-derive-request))
                         ((common-lisp:or network-identifier public-key
                                          metadata)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key* "public_key")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref public-key common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse public-key))
                                   public-key)))
                                (common-lisp:when metadata
                                  (com.inuoe.jzon:write-key* "metadata")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref metadata
                                                     (common-lisp:or
                                                      openapi-generator::json-object
                                                      common-lisp:hash-table))))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/mempool/transaction
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-mempool-transaction-request network-identifier
                     transaction-identifier content)
   "Operation-id: rosetta-mempool-transaction
Summary: Get a Mempool Transaction
Description: Retrieves transaction details from the mempool for a given transaction id from a given network."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/mempool/transaction")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-mempool-transaction-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-mempool-transaction-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-mempool-transaction-request)
                                          rosetta-mempool-transaction-request))
                         ((common-lisp:or network-identifier
                                          transaction-identifier)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key*
                                 "transaction_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref transaction-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse
                                     transaction-identifier))
                                   transaction-identifier)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/mempool
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-mempool-request network-identifier metadata
                     content)
   "Operation-id: rosetta-mempool
Summary: Get All Mempool Transactions
Description: Retrieves a list of transactions currently in the mempool for a given network."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/mempool")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-mempool-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-mempool-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-mempool-request)
                                          rosetta-mempool-request))
                         ((common-lisp:or network-identifier metadata)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (common-lisp:when metadata
                                  (com.inuoe.jzon:write-key* "metadata")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref metadata
                                                     (common-lisp:or
                                                      openapi-generator::json-object
                                                      common-lisp:hash-table))))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/block/transaction
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-block-transaction-request network-identifier
                     block-identifier transaction-identifier content)
   "Operation-id: rosetta-block-transaction
Summary: Get a Block Transaction
Description: Retrieves a Transaction included in a block that is not returned in a BlockResponse."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/block/transaction")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-block-transaction-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-block-transaction-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-block-transaction-request)
                                          rosetta-block-transaction-request))
                         ((common-lisp:or network-identifier block-identifier
                                          transaction-identifier)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key* "block_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref block-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse block-identifier))
                                   block-identifier)))
                                (com.inuoe.jzon:write-key*
                                 "transaction_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref transaction-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse
                                     transaction-identifier))
                                   transaction-identifier)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/block
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-block-request network-identifier block-identifier
                     content)
   "Operation-id: rosetta-block
Summary: Get a Block
Description: Retrieves the Block information for a given block identifier including a list of all transactions in the block."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/block")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-block-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-block-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-block-request)
                                          rosetta-block-request))
                         ((common-lisp:or network-identifier block-identifier)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key* "block_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref block-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse block-identifier))
                                   block-identifier)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/account/balance
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-account-balance-request network-identifier
                     account-identifier block-identifier content)
   "Operation-id: rosetta-account-balance
Summary: Get an Account Balance
Description: An AccountBalanceRequest is utilized to make a balance request on the /account/balance endpoint.
If the block_identifier is populated, a historical balance query should be performed."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/account/balance")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-account-balance-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-account-balance-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-account-balance-request)
                                          rosetta-account-balance-request))
                         ((common-lisp:or network-identifier account-identifier
                                          block-identifier)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (com.inuoe.jzon:write-key* "account_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref account-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse account-identifier))
                                   account-identifier)))
                                (common-lisp:when block-identifier
                                  (com.inuoe.jzon:write-key*
                                   "block_identifier")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref block-identifier
                                                     common-lisp:t)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/network/status
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-status-request network-identifier metadata
                     content)
   "Operation-id: rosetta-network-status
Summary: Get Network Status
Description: Retrieves the current status of the network requested.
Any NetworkIdentifier returned by /network/list should be accessible here."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/network/status")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-status-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-status-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-status-request)
                                          rosetta-status-request))
                         ((common-lisp:or network-identifier metadata)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (common-lisp:when metadata
                                  (com.inuoe.jzon:write-key* "metadata")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref metadata
                                                     (common-lisp:or
                                                      openapi-generator::json-object
                                                      common-lisp:hash-table))))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/network/options
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     rosetta-options-request network-identifier metadata
                     content)
   "Operation-id: rosetta-network-options
Summary: Get Network Options
Description: Retrieves the version information and allowed network-specific types for a NetworkIdentifier.
Any NetworkIdentifier returned by /network/list should be accessible here.
Because options are retrievable in the context of a NetworkIdentifier, it is possible to define unique options for each network."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/network/options")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (rosetta-options-request
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             rosetta-options-request
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           rosetta-options-request)
                                          rosetta-options-request))
                         ((common-lisp:or network-identifier metadata)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "network_identifier")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref network-identifier
                                                    common-lisp:t)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse network-identifier))
                                   network-identifier)))
                                (common-lisp:when metadata
                                  (com.inuoe.jzon:write-key* "metadata")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref metadata
                                                     (common-lisp:or
                                                      openapi-generator::json-object
                                                      common-lisp:hash-table))))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/rosetta/v1/network/list
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: rosetta-network-list
Summary: Get List of Available Networks
Description: Retrieves a list of NetworkIdentifiers that the Rosetta server supports."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/rosetta/v1/network/list")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/search/{id}
                    (id
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     include-metadata)
   "Operation-id: search-by-id
Summary: Search
Description: Search blocks, transactions, contracts, or accounts by hash/ID"
   (serapeum:assuref id common-lisp:string)
   (common-lisp:when include-metadata
     (serapeum:assuref include-metadata common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/search/"
                                                     id))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "include_metadata"
                                                             include-metadata))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v2/pox
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-pox-info
Summary: Get Proof-of-Transfer details
Description: Retrieves Proof-of-Transfer (PoX) information. Can be used for Stacking."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/v2/pox")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/stx_supply/legacy_format
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     height)
   "Operation-id: get-total-stx-supply-legacy-format
Summary: Get total and unlocked STX supply (results formatted the same as the legacy 1.0 API)
Description: Retrieves total supply of STX tokens including those currently in circulation that have been unlocked.
**Note:** this uses the estimated future total supply for the year 2050."
   (common-lisp:when height (serapeum:assuref height common-lisp:number))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/stx_supply/legacy_format")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "height" height))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/stx_supply/circulating/plain
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-stx-supply-circulating-plain
Summary: Get circulating STX supply in plain text format
Description: Retrieves the STX tokens currently in circulation that have been unlocked as plain text."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/stx_supply/circulating/plain")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:case parse
       (:json (com.inuoe.jzon:parse response))
       (common-lisp:otherwise response))))
 (common-lisp:defun get/extended/v1/stx_supply/total/plain
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-stx-supply-total-supply-plain
Summary: Get total STX supply in plain text format
Description: Retrieves the total supply for STX tokens as plain text.
**Note:** this uses the estimated future total supply for the year 2050."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/stx_supply/total/plain")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:case parse
       (:json (com.inuoe.jzon:parse response))
       (common-lisp:otherwise response))))
 (common-lisp:defun get/extended/v1/stx_supply
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     height)
   "Operation-id: get-stx-supply
Summary: Get total and unlocked STX supply
Description: Retrieves the total and unlocked STX supply. More information on Stacking can be found [here] (https://docs.stacks.co/understand-stacks/stacking).
**Note:** This uses the estimated future total supply for the year 2050."
   (common-lisp:when height (serapeum:assuref height common-lisp:number))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/stx_supply")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "height" height))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/info/network_block_time/{network}
                    (network
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-network-block-time-by-network
Summary: Get a given network's target block time
Description: Retrieves the target block time for a given network. The network can be mainnet or testnet. The block time is hardcoded and will change throughout the implementation phases of the testnet."
   (serapeum:assuref network common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat
                                          "/extended/v1/info/network_block_time/"
                                          network))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/info/network_block_times
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-network-block-times
Summary: Get the network target block time
Description: Retrieves the target block times for mainnet and testnet. The block time is hardcoded and will change throughout the implementation phases of the testnet."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/info/network_block_times")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-status
Summary: API status
Description: Retrieves the running status of the Stacks Blockchain API, including the server version and current chain tip information."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v2/info
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-core-api-info
Summary: Get Core API info
Description: Retrieves information about the Core API including the server version"
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/v2/info")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v2/fees/transfer
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-fee-transfer
Summary: Get estimated fee
Description: Retrieves an estimated fee rate for STX transfer transactions. This a a fee rate / byte, and is returned as a JSON integer"
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/v2/fees/transfer")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v2/accounts/{principal}
                    (principal
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) proof
                     tip)
   "Operation-id: get-account-info
Summary: Get account info
Description: Retrieves the account data for a given Account or a Contract Identifier

Where balance is the hex encoding of a unsigned 128-bit integer (big-endian), nonce is an unsigned 64-bit integer, and the proofs are provided as hex strings.

For non-existent accounts, this does not return a 404 error, rather it returns an object with balance and nonce of 0."
   (serapeum:assuref principal common-lisp:string)
   (common-lisp:when proof (serapeum:assuref proof common-lisp:integer))
   (common-lisp:when tip (serapeum:assuref tip common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v2/accounts/"
                                                     principal))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "proof" proof)
                                           (common-lisp:cons "tip" tip))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/address/{principal}/stx_inbound
                    (principal
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset height unanchored until-block)
   "Operation-id: get-account-inbound
Summary: Get inbound STX transfers
Description: Retrieves a list of STX transfers with memos to the given principal. This includes regular transfers from a stx-transfer transaction type,
and transfers from contract-call transactions a the `send-many-memo` bulk sending contract."
   (serapeum:assuref principal common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when height (serapeum:assuref height common-lisp:number))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:when until-block
     (serapeum:assuref until-block common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/address/"
                                                     principal "/stx_inbound"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "height" height)
                                           (common-lisp:cons "unanchored"
                                                             unanchored)
                                           (common-lisp:cons "until_block"
                                                             until-block))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/address/{principal}/assets
                    (principal
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset unanchored until-block)
   "Operation-id: get-account-assets
Summary: Get account assets
Description: Retrieves a list of all assets events associated with an account or a Contract Identifier. This includes Transfers, Mints."
   (serapeum:assuref principal common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:when until-block
     (serapeum:assuref until-block common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/address/"
                                                     principal "/assets"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "unanchored"
                                                             unanchored)
                                           (common-lisp:cons "until_block"
                                                             until-block))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/address/{principal}/nonces
                    (principal
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     block-height block-hash)
   "Operation-id: get-account-nonces
Summary: Get the latest nonce used by an account
Description: Retrieves the latest nonce values used by an account by inspecting the mempool, microblock transactions, and anchored transactions."
   (serapeum:assuref principal common-lisp:string)
   (common-lisp:when block-height
     (serapeum:assuref block-height common-lisp:number))
   (common-lisp:when block-hash
     (serapeum:assuref block-hash common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/address/"
                                                     principal "/nonces"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "block_height"
                                                             block-height)
                                           (common-lisp:cons "block_hash"
                                                             block-hash))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/address/{principal}/transactions_with_transfers
                    (principal
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset height unanchored until-block)
   "Operation-id: get-account-transactions-with-transfers
Summary: Get account transactions including STX transfers for each transaction.
Description: Retrieve all transactions for an account or contract identifier including STX transfers for each transaction."
   (serapeum:assuref principal common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when height (serapeum:assuref height common-lisp:number))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:when until-block
     (serapeum:assuref until-block common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/address/"
                                                     principal
                                                     "/transactions_with_transfers"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "height" height)
                                           (common-lisp:cons "unanchored"
                                                             unanchored)
                                           (common-lisp:cons "until_block"
                                                             until-block))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/address/{principal}/{tx_id}/with_transfers
                    (principal tx-id
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-single-transaction-with-transfers
Summary: Get account transaction information for specific transaction
Description: Retrieves transaction details for a given Transaction Id `tx_id`, for a given account or contract Identifier."
   (serapeum:assuref principal common-lisp:string)
   (serapeum:assuref tx-id common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/address/"
                                                     principal "/" tx-id
                                                     "/with_transfers"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/address/{principal}/transactions
                    (principal
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset height unanchored until-block)
   "Operation-id: get-account-transactions
Summary: Get account transactions
Description: Retrieves a list of all Transactions for a given Address or Contract Identifier. More information on Transaction types can be found [here](https://docs.stacks.co/understand-stacks/transactions#types).

If you need to actively monitor new transactions for an address or contract id, we highly recommend subscribing to [WebSockets or Socket.io](https://github.com/hirosystems/stacks-blockchain-api/tree/master/client) for real-time updates."
   (serapeum:assuref principal common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when height (serapeum:assuref height common-lisp:number))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:when until-block
     (serapeum:assuref until-block common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/address/"
                                                     principal
                                                     "/transactions"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "height" height)
                                           (common-lisp:cons "unanchored"
                                                             unanchored)
                                           (common-lisp:cons "until_block"
                                                             until-block))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/address/{principal}/stx
                    (principal
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     unanchored until-block)
   "Operation-id: get-account-stx-balance
Summary: Get account STX balance
Description: Retrieves STX token balance for a given Address or Contract Identifier."
   (serapeum:assuref principal common-lisp:string)
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:when until-block
     (serapeum:assuref until-block common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/address/"
                                                     principal "/stx"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "unanchored"
                                                             unanchored)
                                           (common-lisp:cons "until_block"
                                                             until-block))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/address/{principal}/balances
                    (principal
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     unanchored until-block)
   "Operation-id: get-account-balance
Summary: Get account balances
Description: Retrieves total account balance information for a given Address or Contract Identifier. This includes the balances of  STX Tokens, Fungible Tokens and Non-Fungible Tokens for the account."
   (serapeum:assuref principal common-lisp:string)
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:when until-block
     (serapeum:assuref until-block common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/address/"
                                                     principal "/balances"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "unanchored"
                                                             unanchored)
                                           (common-lisp:cons "until_block"
                                                             until-block))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/v2/contracts/call-read/{contract_address}/{contract_name}/{function_name}
                    (contract-address contract-name function-name
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     read-only-function-args sender arguments content tip)
   "Operation-id: call-read-only-function
Summary: Call read-only function
Description: Call a read-only public function on a given smart contract.

The smart contract and function are specified using the URL path. The arguments and the simulated tx-sender are supplied via the POST body in the following JSON format:"
   (serapeum:assuref contract-address common-lisp:string)
   (serapeum:assuref contract-name common-lisp:string)
   (serapeum:assuref function-name common-lisp:string)
   (common-lisp:when tip (serapeum:assuref tip common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v2/contracts/call-read/"
                                                     contract-address "/"
                                                     contract-name "/"
                                                     function-name))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "tip" tip))
                                          (common-lisp:when query query)))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         (read-only-function-args
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref
                                             read-only-function-args
                                             (common-lisp:or
                                              openapi-generator::json-object
                                              common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify
                                           read-only-function-args)
                                          read-only-function-args))
                         ((common-lisp:or sender arguments)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (com.inuoe.jzon:write-key* "sender")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref sender common-lisp:string)
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse sender))
                                   sender)))
                                (com.inuoe.jzon:write-key* "arguments")
                                (com.inuoe.jzon:write-value*
                                 (common-lisp:progn
                                  (serapeum:assuref arguments
                                                    (common-lisp:or
                                                     openapi-generator::json-array
                                                     common-lisp:list))
                                  (common-lisp:or
                                   (common-lisp:ignore-errors
                                    (com.inuoe.jzon:parse arguments))
                                   arguments)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v2/contracts/source/{contract_address}/{contract_name}
                    (contract-address contract-name
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) proof
                     tip)
   "Operation-id: get-contract-source
Summary: Get contract source
Description: Retrieves the Clarity source code of a given contract, along with the block height it was published in, and the MARF proof for the data"
   (serapeum:assuref contract-address common-lisp:string)
   (serapeum:assuref contract-name common-lisp:string)
   (common-lisp:when proof (serapeum:assuref proof common-lisp:integer))
   (common-lisp:when tip (serapeum:assuref tip common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v2/contracts/source/"
                                                     contract-address "/"
                                                     contract-name))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "proof" proof)
                                           (common-lisp:cons "tip" tip))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/v2/map_entry/{contract_address}/{contract_name}/{map_name}
                    (contract-address contract-name map-name
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     content proof tip)
   "Operation-id: get-contract-data-map-entry
Summary: Get specific data-map inside a contract
Description: Attempt to fetch data from a contract data map. The contract is identified with Stacks Address `contract_address` and Contract Name `contract_address` in the URL path. The map is identified with [Map Name].

The key to lookup in the map is supplied via the POST body. This should be supplied as the hex string serialization of the key (which should be a Clarity value). Note, this is a JSON string atom.

In the response, `data` is the hex serialization of the map response. Note that map responses are Clarity option types, for non-existent values, this is a serialized none, and for all other responses, it is a serialized (some ...) object."
   (serapeum:assuref contract-address common-lisp:string)
   (serapeum:assuref contract-name common-lisp:string)
   (serapeum:assuref map-name common-lisp:string)
   (common-lisp:when proof (serapeum:assuref proof common-lisp:integer))
   (common-lisp:when tip (serapeum:assuref tip common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v2/map_entry/"
                                                     contract-address "/"
                                                     contract-name "/"
                                                     map-name))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "proof" proof)
                                           (common-lisp:cons "tip" tip))
                                          (common-lisp:when query query)))))
                        :content
                        (common-lisp:cond
                         (content
                          (serapeum:assuref content common-lisp:string)))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/v2/contracts/interface/{contract_address}/{contract_name}
                    (contract-address contract-name
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) tip)
   "Operation-id: get-contract-interface
Summary: Get contract interface
Description: Retrieves a contract interface with a given `contract_address` and `contract name`"
   (serapeum:assuref contract-address common-lisp:string)
   (serapeum:assuref contract-name common-lisp:string)
   (common-lisp:when tip (serapeum:assuref tip common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/v2/contracts/interface/"
                                                     contract-address "/"
                                                     contract-name))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "tip" tip))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/contract/{contract_id}/events
                    (contract-id
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset unanchored)
   "Operation-id: get-contract-events-by-id
Summary: Get contract events
Description: Retrieves a list of events that have been triggered by a given `contract_id`"
   (serapeum:assuref contract-id common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/contract/"
                                                     contract-id "/events"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "unanchored"
                                                             unanchored))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/contract/by_trait
                    (trait-abi
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-contracts-by-trait
Summary: Get contracts by trait
Description: Retrieves a list of contracts based on the following traits listed in JSON format -  functions, variables, maps, fungible tokens and non-fungible tokens"
   (serapeum:assuref trait-abi common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/contract/by_trait")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "trait_abi"
                                                             trait-abi)
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/contract/{contract_id}
                    (contract-id
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     unanchored)
   "Operation-id: get-contract-by-id
Summary: Get contract info
Description: Retrieves details of a contract with a given `contract_id`"
   (serapeum:assuref contract-id common-lisp:string)
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/contract/"
                                                     contract-id))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "unanchored"
                                                             unanchored))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/burnchain/rewards/{address}/total
                    (address
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-burnchain-rewards-total-by-address
Summary: Get total burnchain rewards for the given recipient
Description: Retrieves the total burnchain (e.g. Bitcoin) rewards for a given recipient `address`"
   (serapeum:assuref address common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat
                                          "/extended/v1/burnchain/rewards/"
                                          address "/total"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/burnchain/rewards/{address}
                    (address
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-burnchain-reward-list-by-address
Summary: Get recent burnchain reward for the given recipient
Description: Retrieves a list of recent burnchain (e.g. Bitcoin) rewards for the given recipient with the associated amounts and block info"
   (serapeum:assuref address common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat
                                          "/extended/v1/burnchain/rewards/"
                                          address))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/burnchain/rewards
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-burnchain-reward-list
Summary: Get recent burnchain reward recipients
Description: Retrieves a list of recent burnchain (e.g. Bitcoin) reward recipients with the associated amounts and block info"
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/burnchain/rewards")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/burnchain/reward_slot_holders/{address}
                    (address
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-burnchain-reward-slot-holders-by-address
Summary: Get recent reward slot holder entries for the given address
Description: Retrieves a list of the Bitcoin addresses that would validly receive Proof-of-Transfer commitments for a given reward slot holder recipient address."
   (serapeum:assuref address common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat
                                          "/extended/v1/burnchain/reward_slot_holders/"
                                          address))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/burnchain/reward_slot_holders
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-burnchain-reward-slot-holders
Summary: Get recent reward slot holders
Description: Retrieves a list of the Bitcoin addresses that would validly receive Proof-of-Transfer commitments."
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/burnchain/reward_slot_holders")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/block/by_burn_block_height/{burn_block_height}
                    (burn-block-height
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-block-by-burn-block-height
Summary: Get block by burnchain height
Description: **NOTE:** This endpoint is deprecated in favor of [Get blocks](/api/get-blocks).

Retrieves block details of a specific block for a given burn chain height"
   (serapeum:assuref burn-block-height common-lisp:number)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat
                                          "/extended/v1/block/by_burn_block_height/"
                                          (common-lisp:write-to-string
                                           burn-block-height)))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/block/by_burn_block_hash/{burn_block_hash}
                    (burn-block-hash
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-block-by-burn-block-hash
Summary: Get block by burnchain block hash
Description: **NOTE:** This endpoint is deprecated in favor of [Get blocks](/api/get-blocks).

Retrieves block details of a specific block for a given burnchain block hash"
   (serapeum:assuref burn-block-hash common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat
                                          "/extended/v1/block/by_burn_block_hash/"
                                          burn-block-hash))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/block/by_height/{height}
                    (height
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-block-by-height
Summary: Get block by height
Description: **NOTE:** This endpoint is deprecated in favor of [Get block](/api/get-block).

Retrieves block details of a specific block at a given block height"
   (serapeum:assuref height common-lisp:number)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat
                                          "/extended/v1/block/by_height/"
                                          (common-lisp:write-to-string
                                           height)))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/block/{hash}
                    (hash
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-block-by-hash
Summary: Get block by hash
Description: **NOTE:** This endpoint is deprecated in favor of [Get block](/api/get-block).

Retrieves block details of a specific block for a given chain height. You can use the hash from your latest block ('get_block_list' API) to get your block details."
   (serapeum:assuref hash common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/block/"
                                                     hash))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/block
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-block-list
Summary: Get recent blocks
Description: **NOTE:** This endpoint is deprecated in favor of [Get blocks](/api/get-blocks).

Retrieves a list of recently mined blocks

If you need to actively monitor new blocks, we highly recommend subscribing to [WebSockets or Socket.io](https://github.com/hirosystems/stacks-blockchain-api/tree/master/client) for real-time updates."
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/block")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/pox/cycles/{cycle_number}/signers/{signer_key}/stackers
                    (cycle-number signer-key
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-pox-cycle-signer-stackers
Summary: Get stackers for signer in PoX cycle
Description: Retrieves a list of stackers for a signer in a PoX cycle"
   (serapeum:assuref cycle-number common-lisp:integer)
   (serapeum:assuref signer-key common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v2/pox/cycles/"
                                                     (common-lisp:write-to-string
                                                      cycle-number)
                                                     "/signers/" signer-key
                                                     "/stackers"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/pox/cycles/{cycle_number}/signers/{signer_key}
                    (cycle-number signer-key
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-pox-cycle-signer
Summary: Get signer in PoX cycle
Description: Retrieves details for a signer in a PoX cycle"
   (serapeum:assuref cycle-number common-lisp:integer)
   (serapeum:assuref signer-key common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v2/pox/cycles/"
                                                     (common-lisp:write-to-string
                                                      cycle-number)
                                                     "/signers/" signer-key))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/pox/cycles/{cycle_number}/signers
                    (cycle-number
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-pox-cycle-signers
Summary: Get signers in PoX cycle
Description: Retrieves a list of signers in a PoX cycle"
   (serapeum:assuref cycle-number common-lisp:integer)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v2/pox/cycles/"
                                                     (common-lisp:write-to-string
                                                      cycle-number)
                                                     "/signers"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/pox/cycles/{cycle_number}
                    (cycle-number
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-pox-cycle
Summary: Get PoX cycle
Description: Retrieves details for a PoX cycle"
   (serapeum:assuref cycle-number common-lisp:integer)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v2/pox/cycles/"
                                                     (common-lisp:write-to-string
                                                      cycle-number)))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/pox/cycles
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-pox-cycles
Summary: Get PoX cycles
Description: Retrieves a list of PoX cycles"
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v2/pox/cycles")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/smart-contracts/status
                    (contract-id
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-smart-contracts-status
Summary: Get smart contracts status
Description: Retrieves the deployment status of multiple smart contracts."
   (serapeum:assuref contract-id common-lisp:array)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v2/smart-contracts/status")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "contract_id"
                                                             contract-id))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/addresses/{address}/transactions/{tx_id}/events
                    (address tx-id
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-address-transaction-events
Summary: Get events for an address transaction
Description: Retrieves a paginated list of all STX, FT and NFT events concerning a STX address or Smart Contract ID within a specific transaction."
   (serapeum:assuref address common-lisp:string)
   (serapeum:assuref tx-id common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v2/addresses/"
                                                     address "/transactions/"
                                                     tx-id "/events"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/addresses/{address}/transactions
                    (address
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-address-transactions
Summary: Get address transactions
Description: Retrieves a paginated list of confirmed transactions sent or received by a STX address or Smart Contract ID, alongside the total amount of STX sent or received and the number of STX, FT and NFT transfers contained within each transaction.

More information on Transaction types can be found [here](https://docs.stacks.co/understand-stacks/transactions#types)."
   (serapeum:assuref address common-lisp:string)
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v2/addresses/"
                                                     address "/transactions"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/blocks/{height_or_hash}/transactions
                    (height-or-hash
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-transactions-by-block
Summary: Get transactions by block
Description: Retrieves transactions confirmed in a single block"
   (serapeum:assuref height-or-hash
                     (common-lisp:or common-lisp:integer common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v2/blocks/"
                                                     (common-lisp:format
                                                      common-lisp:nil "~A"
                                                      height-or-hash)
                                                     "/transactions"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/blocks/{height_or_hash}
                    (height-or-hash
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-block
Summary: Get block
Description: Retrieves a single block"
   (serapeum:assuref height-or-hash
                     (common-lisp:or common-lisp:integer common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v2/blocks/"
                                                     (common-lisp:format
                                                      common-lisp:nil "~A"
                                                      height-or-hash)))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/blocks/average-times
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-average-block-times
Summary: Get average block times
Description: Retrieves average block times (in seconds)"
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v2/blocks/average-times")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/blocks
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-blocks
Summary: Get blocks
Description: Retrieves a list of recently mined blocks"
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v2/blocks")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/burn-blocks/{height_or_hash}/blocks
                    (height-or-hash
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-blocks-by-burn-block
Summary: Get blocks by burn block
Description: Retrieves a list of blocks confirmed by a specific burn block"
   (serapeum:assuref height-or-hash
                     (common-lisp:or common-lisp:integer common-lisp:string))
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat
                                          "/extended/v2/burn-blocks/"
                                          (common-lisp:format common-lisp:nil
                                                              "~A"
                                                              height-or-hash)
                                          "/blocks"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/burn-blocks/{height_or_hash}
                    (height-or-hash
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-burn-block
Summary: Get burn block
Description: Retrieves a single burn block"
   (serapeum:assuref height-or-hash
                     (common-lisp:or common-lisp:integer common-lisp:string))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat
                                          "/extended/v2/burn-blocks/"
                                          (common-lisp:format common-lisp:nil
                                                              "~A"
                                                              height-or-hash)))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/burn-blocks
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-burn-blocks
Summary: Get burn blocks
Description: Retrieves a list of recent burn blocks"
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v2/burn-blocks")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/microblock/unanchored/txs
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-unanchored-txs
Summary: Get the list of current transactions that belong to unanchored microblocks
Description: Retrieves transactions that have been streamed in microblocks but not yet accepted or rejected in an anchor block"
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/microblock/unanchored/txs")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/microblock/{hash}
                    (hash
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-microblock-by-hash
Summary: Get microblock
Description: Retrieves a specific microblock by `hash`"
   (serapeum:assuref hash common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/microblock/"
                                                     hash))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/microblock
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-microblock-list
Summary: Get recent microblocks
Description: Retrieves a list of microblocks.

If you need to actively monitor new microblocks, we highly recommend subscribing to [WebSockets or Socket.io](https://github.com/hirosystems/stacks-blockchain-api/tree/master/client) for real-time updates."
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/microblock")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/v2/transactions
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     content)
   "Operation-id: post-core-node-transactions
Summary: Broadcast raw transaction
Description: Broadcasts raw transactions on the network. You can use the [@stacks/transactions](https://github.com/blockstack/stacks.js) project to generate a raw transaction payload."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/v2/transactions")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :content content :method 'post :bearer-auth bearer
                        :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type"
                                             "application/octet-stream")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tx/{tx_id}/raw
                    (tx-id
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-raw-transaction-by-id
Summary: Get Raw Transaction
Description: Retrieves a hex encoded serialized transaction for a given ID"
   (serapeum:assuref tx-id common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/tx/" tx-id
                                                     "/raw"))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tx/{tx_id}
                    (tx-id
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     event-offset event-limit unanchored)
   "Operation-id: get-transaction-by-id
Summary: Get transaction
Description: Retrieves transaction details for a given transaction ID

`import type { Transaction } from '@stacks/stacks-blockchain-api-types';`"
   (serapeum:assuref tx-id common-lisp:string)
   (common-lisp:when event-offset
     (serapeum:assuref event-offset common-lisp:integer))
   (common-lisp:when event-limit
     (serapeum:assuref event-limit common-lisp:integer))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         (str:concat "/extended/v1/tx/" tx-id))
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "event_offset"
                                                             event-offset)
                                           (common-lisp:cons "event_limit"
                                                             event-limit)
                                           (common-lisp:cons "unanchored"
                                                             unanchored))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tx/multiple
                    (tx-id
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     event-offset event-limit unanchored)
   "Operation-id: get-tx-list-details
Summary: Get list of details for transactions
Description: Retrieves a list of transactions for a given list of transaction IDs

If using TypeScript, import typings for this response from our types package:

`import type { Transaction } from '@stacks/stacks-blockchain-api-types';`"
   (serapeum:assuref tx-id common-lisp:array)
   (common-lisp:when event-offset
     (serapeum:assuref event-offset common-lisp:integer))
   (common-lisp:when event-limit
     (serapeum:assuref event-limit common-lisp:integer))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/tx/multiple")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "tx_id" tx-id)
                                           (common-lisp:cons "event_offset"
                                                             event-offset)
                                           (common-lisp:cons "event_limit"
                                                             event-limit)
                                           (common-lisp:cons "unanchored"
                                                             unanchored))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tx/mempool/stats
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-mempool-transaction-stats
Summary: Get statistics for mempool transactions
Description: Queries for transactions counts, age (by block height), fees (simple average), and size.
All results broken down by transaction type and percentiles (p25, p50, p75, p95)."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/tx/mempool/stats")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tx/mempool/dropped
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset)
   "Operation-id: get-dropped-mempool-transaction-list
Summary: Get dropped mempool transactions
Description: Retrieves all recently-broadcast transactions that have been dropped from the mempool.

Transactions are dropped from the mempool if:
 * they were stale and awaiting garbage collection or,
 * were expensive,  or
 * were replaced with a new fee"
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/tx/mempool/dropped")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v2/mempool/fees
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*))
   "Operation-id: get-mempool-fee-priorities
Summary: Get mempool transaction fee priorities
Description: Returns estimated fee priorities (in micro-STX) for all transactions that are currently in the mempool. Also returns priorities separated by transaction type."
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v2/mempool/fees")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:when query query))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tx/mempool
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     sender-address recipient-address address order-by order
                     limit offset unanchored)
   "Operation-id: get-mempool-transaction-list
Summary: Get mempool transactions
Description: Retrieves all transactions that have been recently broadcast to the mempool. These are pending transactions awaiting confirmation.

If you need to monitor new transactions, we highly recommend subscribing to [WebSockets or Socket.io](https://github.com/hirosystems/stacks-blockchain-api/tree/master/client) for real-time updates."
   (common-lisp:when sender-address
     (serapeum:assuref sender-address common-lisp:string))
   (common-lisp:when recipient-address
     (serapeum:assuref recipient-address common-lisp:string))
   (common-lisp:when address (serapeum:assuref address common-lisp:string))
   (common-lisp:when order-by (serapeum:assuref order-by common-lisp:string))
   (common-lisp:when order (serapeum:assuref order common-lisp:string))
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/tx/mempool")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "sender_address"
                                                             sender-address)
                                           (common-lisp:cons
                                            "recipient_address"
                                            recipient-address)
                                           (common-lisp:cons "address" address)
                                           (common-lisp:cons "order_by"
                                                             order-by)
                                           (common-lisp:cons "order" order)
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "unanchored"
                                                             unanchored))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun get/extended/v1/tx
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*) limit
                     offset type unanchored)
   "Operation-id: get-transaction-list
Summary: Get recent transactions
Description: Retrieves all recently mined transactions

If using TypeScript, import typings for this response from our types package:

`import type { TransactionResults } from '@stacks/stacks-blockchain-api-types';`"
   (common-lisp:when limit (serapeum:assuref limit common-lisp:integer))
   (common-lisp:when offset (serapeum:assuref offset common-lisp:integer))
   (common-lisp:when type (serapeum:assuref type common-lisp:array))
   (common-lisp:when unanchored
     (serapeum:assuref unanchored common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/tx")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "limit" limit)
                                           (common-lisp:cons "offset" offset)
                                           (common-lisp:cons "type" type)
                                           (common-lisp:cons "unanchored"
                                                             unanchored))
                                          (common-lisp:when query query)))))
                        :method 'get :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/extended/v1/faucets/btc
                    (
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     address content)
   "Operation-id: run-faucet-btc
Summary: Add testnet BTC tokens to address
Description: Add 1 BTC token to the specified testnet BTC address.

The endpoint returns the transaction ID, which you can use to view the transaction in a testnet Bitcoin block
explorer. The tokens are delivered once the transaction has been included in a block.

**Note:** This is a testnet only endpoint. This endpoint will not work on the mainnet."
   (serapeum:assuref address common-lisp:string)
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/faucets/btc")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "address"
                                                             address))
                                          (common-lisp:when query query)))))
                        :content
                        (common-lisp:cond
                         (content
                          (common-lisp:if (common-lisp:not
                                           (common-lisp:stringp
                                            (serapeum:assuref content
                                                              (common-lisp:or
                                                               openapi-generator::json-object
                                                               common-lisp:hash-table))))
                                          (com.inuoe.jzon:stringify content)
                                          content))
                         ((common-lisp:or address)
                          (common-lisp:let ((s
                                             (common-lisp:make-string-output-stream)))
                            (common-lisp:declare (common-lisp:stream s))
                            (com.inuoe.jzon:with-writer* (:stream s :pretty
                                                          common-lisp:t)
                              (com.inuoe.jzon:with-object*
                                (common-lisp:when address
                                  (com.inuoe.jzon:write-key* "address")
                                  (com.inuoe.jzon:write-value*
                                   (serapeum:assuref address
                                                     common-lisp:string)))))
                            (common-lisp:let ((openapi-generator::output
                                               (common-lisp:get-output-stream-string
                                                s)))
                              (common-lisp:declare
                               (common-lisp:string openapi-generator::output))
                              (common-lisp:unless
                                  (common-lisp:string= "{}"
                                                       openapi-generator::output)
                                openapi-generator::output)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:cons "Content-Type" "application/json")
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:defun post/extended/v1/faucets/stx
                    (address
                     common-lisp:&key (query *query*) (headers *headers*)
                     (cookie *cookie*) (authorization *authorization*)
                     (bearer *bearer*) (server *server*) (parse *parse*)
                     stacking)
   "Operation-id: run-faucet-stx
Summary: Get STX testnet tokens
Description: Add 500 STX tokens to the specified testnet address. Testnet STX addresses begin with `ST`. If the `stacking`
parameter is set to `true`, the faucet will add the required number of tokens for individual stacking to the
specified testnet address.

The endpoint returns the transaction ID, which you can use to view the transaction in the
[Stacks Explorer](https://explorer.hiro.so/?chain=testnet). The tokens are delivered once the transaction has
been included in an anchor block.

A common reason for failed faucet transactions is that the faucet has run out of tokens. If you are experiencing
failed faucet transactions to a testnet address, you can get help in [Discord](https://stacks.chat).

**Note:** This is a testnet only endpoint. This endpoint will not work on the mainnet."
   (serapeum:assuref address common-lisp:string)
   (common-lisp:when stacking (serapeum:assuref stacking common-lisp:boolean))
   (common-lisp:let* ((server-uri
                       (quri.uri:uri
                        (common-lisp:or server "https://api.mainnet.hiro.so")))
                      (response
                       (dexador.backend.usocket:request
                        (quri:render-uri
                         (quri:make-uri :scheme
                                        (quri.uri:uri-scheme server-uri) :host
                                        (quri.uri:uri-host server-uri) :port
                                        (quri.uri:uri-port server-uri) :path
                                        (str:concat
                                         (quri.uri:uri-path server-uri)
                                         "/extended/v1/faucets/stx")
                                        :query
                                        (openapi-generator::remove-empty-values
                                         (common-lisp:append
                                          (common-lisp:list
                                           (common-lisp:cons "address" address)
                                           (common-lisp:cons "stacking"
                                                             stacking))
                                          (common-lisp:when query query)))))
                        :method 'post :bearer-auth bearer :headers
                        (openapi-generator::remove-empty-values
                         (common-lisp:append
                          (common-lisp:list
                           (common-lisp:when authorization
                             (common-lisp:cons "Authorization" authorization))
                           (common-lisp:cons "cookie" cookie))
                          (common-lisp:when headers headers))))))
     (common-lisp:if parse
                     (com.inuoe.jzon:parse response)
                     response)))
 (common-lisp:progn
 (serapeum:defalias get-pool-delegations
   'get/extended/beta/stacking/{pool_principal}/delegations)
 (serapeum:defalias get-filtered-events
   'get/extended/v1/tx/events)
 (serapeum:defalias post-fee-transaction
   'post/v2/fees/transaction)
 (serapeum:defalias fetch-fee-rate
   'post/extended/v1/fee_rate)
 (serapeum:defalias get-nft-mints
   'get/extended/v1/tokens/nft/mints)
 (serapeum:defalias get-nft-history
   'get/extended/v1/tokens/nft/history)
 (serapeum:defalias get-nft-holdings
   'get/extended/v1/tokens/nft/holdings)
 (serapeum:defalias get-address-mempool-transactions
   'get/extended/v1/address/{address}/mempool)
 (serapeum:defalias get-transactions-by-block-height
   'get/extended/v1/tx/block_height/{height})
 (serapeum:defalias get-transactions-by-block-hash
   'get/extended/v1/tx/block/{block_hash})
 (serapeum:defalias get-names-owned-by-address
   'get/v1/addresses/{blockchain}/{address})
 (serapeum:defalias get-historical-zone-file
   'get/v1/names/{name}/zonefile/{zonefilehash})
 (serapeum:defalias fetch-zone-file
   'get/v1/names/{name}/zonefile)
 (serapeum:defalias fetch-subdomains-list-for-name
   'get/v1/names/{name}/subdomains)
 (serapeum:defalias get-name-info
   'get/v1/names/{name})
 (serapeum:defalias get-all-names
   'get/v1/names)
 (serapeum:defalias get-namespace-names
   'get/v1/namespaces/{tld}/names)
 (serapeum:defalias get-all-namespaces
   'get/v1/namespaces)
 (serapeum:defalias get-name-price
   'get/v2/prices/names/{name})
 (serapeum:defalias get-namespace-price
   'get/v2/prices/namespaces/{tld})
 (serapeum:defalias rosetta-construction-combine
   'post/rosetta/v1/construction/combine)
 (serapeum:defalias rosetta-construction-payloads
   'post/rosetta/v1/construction/payloads)
 (serapeum:defalias rosetta-construction-submit
   'post/rosetta/v1/construction/submit)
 (serapeum:defalias rosetta-construction-preprocess
   'post/rosetta/v1/construction/preprocess)
 (serapeum:defalias rosetta-construction-parse
   'post/rosetta/v1/construction/parse)
 (serapeum:defalias rosetta-construction-metadata
   'post/rosetta/v1/construction/metadata)
 (serapeum:defalias rosetta-construction-hash
   'post/rosetta/v1/construction/hash)
 (serapeum:defalias rosetta-construction-derive
   'post/rosetta/v1/construction/derive)
 (serapeum:defalias rosetta-mempool-transaction
   'post/rosetta/v1/mempool/transaction)
 (serapeum:defalias rosetta-mempool
   'post/rosetta/v1/mempool)
 (serapeum:defalias rosetta-block-transaction
   'post/rosetta/v1/block/transaction)
 (serapeum:defalias rosetta-block
   'post/rosetta/v1/block)
 (serapeum:defalias rosetta-account-balance
   'post/rosetta/v1/account/balance)
 (serapeum:defalias rosetta-network-status
   'post/rosetta/v1/network/status)
 (serapeum:defalias rosetta-network-options
   'post/rosetta/v1/network/options)
 (serapeum:defalias rosetta-network-list
   'post/rosetta/v1/network/list)
 (serapeum:defalias search-by-id
   'get/extended/v1/search/{id})
 (serapeum:defalias get-pox-info
   'get/v2/pox)
 (serapeum:defalias get-total-stx-supply-legacy-format
   'get/extended/v1/stx_supply/legacy_format)
 (serapeum:defalias get-stx-supply-circulating-plain
   'get/extended/v1/stx_supply/circulating/plain)
 (serapeum:defalias get-stx-supply-total-supply-plain
   'get/extended/v1/stx_supply/total/plain)
 (serapeum:defalias get-stx-supply
   'get/extended/v1/stx_supply)
 (serapeum:defalias get-network-block-time-by-network
   'get/extended/v1/info/network_block_time/{network})
 (serapeum:defalias get-network-block-times
   'get/extended/v1/info/network_block_times)
 (serapeum:defalias get-status
   'get/extended)
 (serapeum:defalias get-core-api-info
   'get/v2/info)
 (serapeum:defalias get-fee-transfer
   'get/v2/fees/transfer)
 (serapeum:defalias get-account-info
   'get/v2/accounts/{principal})
 (serapeum:defalias get-account-inbound
   'get/extended/v1/address/{principal}/stx_inbound)
 (serapeum:defalias get-account-assets
   'get/extended/v1/address/{principal}/assets)
 (serapeum:defalias get-account-nonces
   'get/extended/v1/address/{principal}/nonces)
 (serapeum:defalias get-account-transactions-with-transfers
   'get/extended/v1/address/{principal}/transactions_with_transfers)
 (serapeum:defalias get-single-transaction-with-transfers
   'get/extended/v1/address/{principal}/{tx_id}/with_transfers)
 (serapeum:defalias get-account-transactions
   'get/extended/v1/address/{principal}/transactions)
 (serapeum:defalias get-account-stx-balance
   'get/extended/v1/address/{principal}/stx)
 (serapeum:defalias get-account-balance
   'get/extended/v1/address/{principal}/balances)
 (serapeum:defalias call-read-only-function
   'post/v2/contracts/call-read/{contract_address}/{contract_name}/{function_name})
 (serapeum:defalias get-contract-source
   'get/v2/contracts/source/{contract_address}/{contract_name})
 (serapeum:defalias get-contract-data-map-entry
   'post/v2/map_entry/{contract_address}/{contract_name}/{map_name})
 (serapeum:defalias get-contract-interface
   'get/v2/contracts/interface/{contract_address}/{contract_name})
 (serapeum:defalias get-contract-events-by-id
   'get/extended/v1/contract/{contract_id}/events)
 (serapeum:defalias get-contracts-by-trait
   'get/extended/v1/contract/by_trait)
 (serapeum:defalias get-contract-by-id
   'get/extended/v1/contract/{contract_id})
 (serapeum:defalias get-burnchain-rewards-total-by-address
   'get/extended/v1/burnchain/rewards/{address}/total)
 (serapeum:defalias get-burnchain-reward-list-by-address
   'get/extended/v1/burnchain/rewards/{address})
 (serapeum:defalias get-burnchain-reward-list
   'get/extended/v1/burnchain/rewards)
 (serapeum:defalias get-burnchain-reward-slot-holders-by-address
   'get/extended/v1/burnchain/reward_slot_holders/{address})
 (serapeum:defalias get-burnchain-reward-slot-holders
   'get/extended/v1/burnchain/reward_slot_holders)
 (serapeum:defalias get-block-by-burn-block-height
   'get/extended/v1/block/by_burn_block_height/{burn_block_height})
 (serapeum:defalias get-block-by-burn-block-hash
   'get/extended/v1/block/by_burn_block_hash/{burn_block_hash})
 (serapeum:defalias get-block-by-height
   'get/extended/v1/block/by_height/{height})
 (serapeum:defalias get-block-by-hash
   'get/extended/v1/block/{hash})
 (serapeum:defalias get-block-list
   'get/extended/v1/block)
 (serapeum:defalias get-pox-cycle-signer-stackers
   'get/extended/v2/pox/cycles/{cycle_number}/signers/{signer_key}/stackers)
 (serapeum:defalias get-pox-cycle-signer
   'get/extended/v2/pox/cycles/{cycle_number}/signers/{signer_key})
 (serapeum:defalias get-pox-cycle-signers
   'get/extended/v2/pox/cycles/{cycle_number}/signers)
 (serapeum:defalias get-pox-cycle
   'get/extended/v2/pox/cycles/{cycle_number})
 (serapeum:defalias get-pox-cycles
   'get/extended/v2/pox/cycles)
 (serapeum:defalias get-smart-contracts-status
   'get/extended/v2/smart-contracts/status)
 (serapeum:defalias get-address-transaction-events
   'get/extended/v2/addresses/{address}/transactions/{tx_id}/events)
 (serapeum:defalias get-address-transactions
   'get/extended/v2/addresses/{address}/transactions)
 (serapeum:defalias get-transactions-by-block
   'get/extended/v2/blocks/{height_or_hash}/transactions)
 (serapeum:defalias get-block
   'get/extended/v2/blocks/{height_or_hash})
 (serapeum:defalias get-average-block-times
   'get/extended/v2/blocks/average-times)
 (serapeum:defalias get-blocks
   'get/extended/v2/blocks)
 (serapeum:defalias get-blocks-by-burn-block
   'get/extended/v2/burn-blocks/{height_or_hash}/blocks)
 (serapeum:defalias get-burn-block
   'get/extended/v2/burn-blocks/{height_or_hash})
 (serapeum:defalias get-burn-blocks
   'get/extended/v2/burn-blocks)
 (serapeum:defalias get-unanchored-txs
   'get/extended/v1/microblock/unanchored/txs)
 (serapeum:defalias get-microblock-by-hash
   'get/extended/v1/microblock/{hash})
 (serapeum:defalias get-microblock-list
   'get/extended/v1/microblock)
 (serapeum:defalias post-core-node-transactions
   'post/v2/transactions)
 (serapeum:defalias get-raw-transaction-by-id
   'get/extended/v1/tx/{tx_id}/raw)
 (serapeum:defalias get-transaction-by-id
   'get/extended/v1/tx/{tx_id})
 (serapeum:defalias get-tx-list-details
   'get/extended/v1/tx/multiple)
 (serapeum:defalias get-mempool-transaction-stats
   'get/extended/v1/tx/mempool/stats)
 (serapeum:defalias get-dropped-mempool-transaction-list
   'get/extended/v1/tx/mempool/dropped)
 (serapeum:defalias get-mempool-fee-priorities
   'get/extended/v2/mempool/fees)
 (serapeum:defalias get-mempool-transaction-list
   'get/extended/v1/tx/mempool)
 (serapeum:defalias get-transaction-list
   'get/extended/v1/tx)
 (serapeum:defalias run-faucet-btc
   'post/extended/v1/faucets/btc)
 (serapeum:defalias run-faucet-stx
   'post/extended/v1/faucets/stx))