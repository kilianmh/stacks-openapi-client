(defsystem "stacks-openapi-client"
  :version "0.0.1"
  :author ""
  :license ""
  :depends-on ("quri"
               "str"
               "com.inuoe.jzon"
               "dexador"
               "uiop"
               "openapi-generator"
               "serapeum")
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description "")

